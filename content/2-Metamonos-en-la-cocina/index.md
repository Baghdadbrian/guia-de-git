#Metámonos en la cocina
Olvídate de códigos, palabras que suenan raro en tu cabeza, símbolos que nunca viste antes y demás jerga que te hacen cerrar la pestaña del navegador despavorido.

Imagínate que **una chef te invita a su cocina.** Una en la que no sólo puedes darte una vuelta, mirar y ya está: se te permite leer sus libros de recetas de primera mano y conocer todos sus secretos. Y mejor todavía, la cocina cuenta con **un sistema Git** por el cuál tu puedes realizar sugerencias de mejora de estas recetas, **copiártelas y llevártelas** a tu cocina digital, **continuar mejorándolas, practicar…** pero sin la posibilidad de cambiar aquello que planteaba la chef de inicio en su receta, a no ser que te lo permita.

Has ido de visita con unas cuantas personas igual de curiosas que tú y han descubierto una fabulosa receta: cómo hacer pasta. La receta consiste en la forma de producción completa de la pasta hasta que está lista para hervir. La receta acaba cuando la hierve y la sirve en el plato. El aspecto vendría a ser una cosa parecida a la foto… ¿Yummy? Si eres de los que crees que le falta algo veamos qué puedes plantear…
