#¿Git qué?
Git (pronunciado “guit” ) es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente.

#Vale, de nuevo...¿Qué?
Este suele ser uno de los problemas por los que herramientas como Git no acaban siendo usadas por quienes, en realidad, le sacarían un jugo tremendo: **el lenguaje.** Si, es difícil explicarlo de otro modo… pero no imposible. En el momento en que logremos, poco a poco, empezar a explicar por analogías mucho de lo que ocurre en internet en el lado de la programación, el código, lo hack y lo geek, seguro que empezaremos a abrir un camino aún inexplorado pero siempre deseado. Tanto para el bien del desarrollo colaborativo, como para el enriquecimiento de miradas por distintos grupos que puedan estar interesados; entonces tomemos el reto:

Permitirme la osadía de intentar dar otro tipo de definición:
*Git es un programa que permite, a quien desee, aportar nuevas ideas a una idea ya existente de forma colaborativa. Todo ello a través de un sistema de control y revisión que permite que puedas elegir si alguna de esas ideas propuestas mejoran la tuya, descartarlas, volver atrás si la acepté y me arrepentí, mezclar posibilidades y hacer las tuyas propias.*

¿Todavía estás buscando a Wally en la ecuación? Vamos a ir un paso más allá e intentar contar lo que podrías hacer con Git como si de la preparación de un plato se tratase y trabajásemos entre varios cocineros.

#Antes un poco de contexto…
Si te interesa saber cómo nace Git y de qué se trata desde un punto de vista técnico, lo vas a encontrar en la página oficial … con ese bonito lema de “Lo distribuido en la nueva centralización”

Y mira si tienes 15 minutos y quieres aprender Git (in english) pues también todo tuyo en modo crudo: https://try.github.io/

Las plataformas más conocidas donde podemos utilizar Git son: GitHub, GitLab y BitBucket. También podemos instalar Git en nuestro propio servidor y no necesitar plataformas de terceros… pero eso para otro día.

Particularmente uso github, una tremenda red desde donde poder compartir tus códigos (esos conjuntos de instrucciones donde le decimos a un programa qué hacer, o a una web qué información mostrarnos), aprender de otros, leer, estudiar, seguir aprendiendo y sobre todo, conectar desde el hacer.
