#Guía de Git para comunicadorex, organizaciones sociales y cocineros en general

![](images/portada-manual-git.jpg)

¿Qué es Git? ¿Cómo puedo usarla para proyectos de colaboración?
Esta guía permite entender a través de una analogía con las recetas y cocina, la propuesta de la herramienta Git, además de brindar un glosario básico para la colaboración a través de esta herramienta.

---
