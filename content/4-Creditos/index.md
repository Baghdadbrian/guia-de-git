#Créditos:
Esta guía es realizada por Daniel Cotillas Ruiz del colectivo Nodo Común (www.nodocomun.org)
El contenido de esta guía está bajo licencia Producción entre Pares (Copyfarleft) derivada de la licencia Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported.

Al dente por Handles | BY NC ND

Spaghetti with Stuffed Roasted Tomatoes and Bacon de esimpraim | BY NC ND

Spaghetti de Rool Paap | CC BY

Tarako Spaghetti de Gaku | CC BY SA

Meatballs & Spaghetti de Matt | CC BY NC
