#Glosario de un recetario colaborativo
---
##- Repositorio
El corazón de cualquier cocina, el lugar donde guardamos nuestras recetas. Un repositorio es cada una de ellas, de forma que podemos acceder para leerlas y estudiarlas. En nuestro repositorio guardamos las nuestras, pero cuando vamos al repositorio de otras chefs podemos realizar varias acciones, desde ponerles una estrella de favorito hasta copiárnoslas en nuestro repositorio para desde ahí plantear los cambios que queramos, eso es, hacer un **fork**.

---
##- Fork
Como no quieres estropearle la receta a la chef y prefieres practicar en tu propia cocina sin tener que esperar su aceptación, metes literalmente el tenedor (fork) y le sacas la receta de hacer pasta. Una vez en tu cocina tienes total libertad para probar todas las variables posibles. Que si ahora tomate, que luego queso, que me arrepiento y le quito el tomate, tal vez sea mejor con crema. Planteamos cambios en nuestra propia receta (commits & push) y cuando hemos dado con lo que realmente queremos proponer a la chef le enviamos una propuesta (pull request). Además, si me arrepiento, siempre puedo volver atrás ya tengo la receta inicial sobre cómo hacer pasta.

---
##- Commit
Decides innovar y lanzarte a la piscina… ¡Crees que estará mejor con albahaca!. Pero tus amigas y amigos no están de acuerdo contigo. Unos creen que mejor tomate, otros que aceite, otros tocino y los más iconoclasistas quieren colocarle piña (¡a la hoguera!).

Cada uno de estos cambios realizados por nuestra parte **es un commit**. En nuestra receta aparecen así pero la receta maestra no ha sido modificada.

---
##- Push
Para poder hacer que los cambios en la receta tengan efecto debemos hacerlos públicos. Con el commit ya hemos hecho la solicitud de inclusión, el push es la acción por la cual los cambios pasan a formar parte de la receta. En nuestras propias recetas (repositorios) hacer un commit y push van acompañados de la mano. Si quisiéramos hacerlo desde la receta de la chef invitada a la que hicimos un fork, sólo ella podrá aceptar los cambios haciendo un push desde la solicitud de cambio. Y esa solicitud se llama **pull request**, ¿y eso es…?

---
##- Pull Request
Queda pendiente que la chef revise y acepte si es que alguna de esas propuestas realmente mejora su receta de pasta. Ella será quién le ponga la albahaca, el aceite, el tocino o la piña, y en qué medida. Estas propuestas son probables peticiones de cambio. Cada una de ellas aparecen como **pull request** para cambiar la receta original (el código fuente).

La chef acepta finalmente la idea de la albahaca después de probarla antes. Agarra el bote y le mete de lleno… Acaba de hacer un pull. Parece buena, continuamos. O tal vez pasado el tiempo se arrepiente, ¡qué dilema!. Como anotó en la agenda (control de revisiones) el momento en que la puso, siempre puede volver al punto de inicio.

---
##- Issue
Cuando hemos estado leyendo la receta nos han surgido muchas dudas, hay un punto entre la ebullición del agua y el momento en que se mete la pasta que no nos queda claro. Entonces, podemos hacer preguntas sobre estas dudas que tenemos a través de los Issue. De esta forma mantenemos un diálogo, tanto entre la chef y yo, como con todas aquellas personas que han querido echar un ojo a la misma receta y tal vez tengan otras soluciones posibles. La cuestión no es arreglarlo entre dos, sino ¡entre todos los que quieren compartir!

---
##- Branches
Nos hemos metido varias personas al mismo tiempo a ver cómo podemos mejorar la receta. Estar trabajando todas al mismo tiempo, sobre los mismos paso a paso (el código) se puede convertir, y lo hace, en un desmadre. Entonces, hacemos una copia temporal desde la misma receta para tomar un camino alternativo, que puede acabar en algo que se sume finalmente a la receta final, o tal vez no. Estos caminos múltiples son las branches (ramas) y nos permiten estar modificando distintas partes de la receta al mismo tiempo por varias personas. Una vez tenemos certeza sobre el buen devenir de nuestra branch, ¿qué hacemos? Pues fusionarlas, es decir, un **merge** :).

---
*¿Quieres mejorar esta receta usando Git?*

*Si quieres echar una mano para mejorarla, desde Nodo Común se ha creado un repositorio donde está la receta: https://nodocomun.github.io/manual-git/
Para poder hacerlo necesitarás justamente poner en juego la terminología que aquí te viene, al tiempo que pruebes, metiendo las manos en la masa, cómo funciona la lógica git y la plataforma de GitHub.*

*También puedes meterle el tenedor desde el código fuente aquí:* https://github.com/nodocomun/nodocomun.github.io

*También puedes plantearte iniciar con tu propia página en GitHub, para ello hemos traducido la web oficial por si no entiendes bien el inglés:*
https://nodocomun.github.io/paginas-github/
